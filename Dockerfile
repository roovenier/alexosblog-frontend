FROM node:12.13-alpine

ENV APP_ROOT /app
ENV NUXT_HOST=0.0.0.0
ENV NUXT_PORT=${PORT}

RUN mkdir /app

WORKDIR /app

COPY . .

RUN npm install && npm run build

CMD ["npm", "run", "start:prod"]