import { getModule }    from 'nuxt-property-decorator';
import { Store }        from 'vuex';
import { RootState }    from '~/store';
import PostsModule      from '~/store/posts';
import PostDetailModule from '~/store/post-detail';
import TagsModule       from '~/store/tags';

let posts: PostsModule;
let postDetail: PostDetailModule;
let tags: TagsModule;

const initialiseStores = (store: Store<RootState>): void => {
  posts = getModule(PostsModule, store);
  postDetail = getModule(PostDetailModule, store);
  tags = getModule(TagsModule, store);
};

export {
  initialiseStores,
  posts,
  postDetail,
  tags
};
