import { ApiModule } from '~/api/types';

let $api: ApiModule;

const initializeApi = (apiModule: ApiModule): void => {
  $api = apiModule;
};

export {
  initializeApi,
  $api
};
