import { ApolloQueryResult } from 'apollo-client';

export interface Tag {
    id: number;
    name: string;
}

export interface GetTagsResponse {
    tags: Array<Tag>;
}
export type GetTagsResponseType = Promise<ApolloQueryResult<GetTagsResponse>>

export interface ApiTags {
    getAllTags: () => GetTagsResponseType;
}