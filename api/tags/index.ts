import gql                              from 'graphql-tag';
import { ApolloClientType }             from '~/api/types';
import { ApiTags, GetTagsResponseType } from '~/api/tags/types';

const methods = (apolloClient: ApolloClientType): ApiTags => ({
  getAllTags(): GetTagsResponseType {
    return apolloClient.query({
      query: gql`
        query {
            tags {
                id
                name
            }
        }`,
      variables: {}
    });
  }
});

export default methods;