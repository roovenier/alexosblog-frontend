import { ApolloClientType, ApiModule } from '~/api/types';
import postsApi                        from '~/api/posts/index';
import tagsApi                         from '~/api/tags/index';

const Api = (apolloClient: ApolloClientType): ApiModule => ({
  posts: postsApi(apolloClient),
  tags : tagsApi(apolloClient)
});

export default Api;