import gql                                                                                    from 'graphql-tag';
import { ApolloClientType }                                                                   from '~/api/types';
import { ApiPosts, GetPostsResponseType, GetPostByIdResponseType, GetPostsByTagResponseType } from '~/api/posts/types';

const methods = (apolloClient: ApolloClientType): ApiPosts => ({
  getPosts({limit, skip}): GetPostsResponseType {
    return apolloClient.query({
      query: gql`
        query ($limit: Int!, $skip: Int!) {
            posts(limit: $limit, skip: $skip) {
                postList {
                    id
                    title
                    createdAt
                    postItems {
                        id
                        title
                        text
                        description
                        lang {
                            id
                            name
                            logo
                        }
                    }
                    tags {
                        id
                        name
                    }
                }
                isLast
            }
        }`,
      variables: {
        limit, skip
      }
    });
  },
  getPostById(postId): GetPostByIdResponseType {
    return apolloClient.query({
      query: gql`
        query ($postId: Int!) {
            postById(postId: $postId) {
                id
                userId
                title
                createdAt
                tags {
                    id
                    name
                }
                postItems {
                    id
                    title
                    description
                    text
                    lang {
                        id
                        name
                        logo
                    }
                }
            }
        }`,
      variables: {
        postId
      }
    });
  },
  getPostsByTag({tagId, limit, skip}): GetPostsByTagResponseType {
    return apolloClient.query({
      query: gql`
        query ($tagId: Int!, $limit: Int!, $skip: Int!) {
            postsByTag(tagId: $tagId, limit: $limit, skip: $skip) {
                postList {
                    id
                    userId
                    title
                    createdAt
                    tags {
                        id
                        name
                    }
                    postItems {
                        id
                        title
                        description
                        text
                        lang {
                            id
                            name
                            logo
                        }
                    }
                }
                isLast
            }
        }`,
      variables: {
        tagId, limit, skip
      }
    });
  }
});

export default methods;