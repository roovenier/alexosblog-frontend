import { ApolloQueryResult } from 'apollo-client';
import { Tag }               from '~/api/tags/types';

export interface PostItem {
    id: number;
    title: string;
    text: string;
    description: string;
    lang: {
        id: number;
        name: string;
        logo: string;
    };
}
export interface Post {
    id: number;
    title: string;
    postItems: Array<PostItem>;
    tags: Array<Tag>;
}

export interface GetPostsResponse {
    posts: {
        postList: Array<Post>;
        isLast: boolean;
    };
}
export type GetPostsResponseType = Promise<ApolloQueryResult<GetPostsResponse>>

export interface GetPostByIdResponse {
    postById: Post;
}
export type GetPostByIdResponseType = Promise<ApolloQueryResult<GetPostByIdResponse>>

export interface GetPostsByTagResponse {
    postsByTag: {
        postList: Array<Post>;
        isLast: boolean;
    };
}
export type GetPostsByTagResponseType = Promise<ApolloQueryResult<GetPostsByTagResponse>>

export interface ApiPosts {
    getPosts: ({limit, skip}: {limit: number; skip: number}) => GetPostsResponseType;
    getPostById: (postId: number) => GetPostByIdResponseType;
    getPostsByTag: ({tagId, limit, skip}: {tagId: number; limit: number; skip: number}) => GetPostsByTagResponseType;
}