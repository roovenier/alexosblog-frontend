import { ApolloClient }  from 'apollo-client';
import { NormalizedCacheObject } from 'apollo-cache-inmemory';
import { ApiPosts }      from '~/api/posts/types';
import { ApiTags }       from '~/api/tags/types';

export type ApolloClientType = ApolloClient<NormalizedCacheObject>;

export interface ApiModule {
    posts: ApiPosts;
    tags: ApiTags;
}  