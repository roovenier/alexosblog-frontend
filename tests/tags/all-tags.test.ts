import { ApolloQueryResult } from 'apollo-client';
import apiModule             from '~/tests/setup';
import { GetTagsResponse }   from '~/api/tags/types';

describe('All tags', () => {
  let response: ApolloQueryResult<GetTagsResponse>;
  let data: GetTagsResponse;

  beforeAll(async() => {
    response = await apiModule.tags.getAllTags();
    data = response.data;
  });

  it('All tags have keys', () => {
    expect(data).toHaveProperty('tags');

    expect(data.tags).toBeInstanceOf(Array);
  });

  it('A single tag', () => {
    data.tags.forEach((tag) => {
      expect(tag).toHaveProperty('id');
      expect(tag).toHaveProperty('name');

      expect(typeof tag.id).toBe('number');
      expect(typeof tag.name).toBe('string');
    });
  });
});