import { ApolloQueryResult }     from 'apollo-client';
import apiModule                 from '~/tests/setup';
import { GetPostsByTagResponse } from '~/api/posts/types';

describe('Posts by tag', () => {
  let response: ApolloQueryResult<GetPostsByTagResponse>;
  let data: GetPostsByTagResponse;

  beforeAll(async() => {
    response = await apiModule.posts.getPostsByTag({ tagId : 2, limit : 10, skip : 0 });
    data = response.data;
  });

  it('Posts by tag have keys', () => {
    expect(data).toHaveProperty('postsByTag');

    expect(data.postsByTag).toHaveProperty('postList');
    expect(data.postsByTag).toHaveProperty('isLast');

    expect(typeof data.postsByTag.isLast).toBe('boolean');
    expect(data.postsByTag.postList).toBeInstanceOf(Array);
  });

  it('A single post', () => {
    data.postsByTag.postList.forEach((posts) => {
      expect(posts).toHaveProperty('id');
      expect(posts).toHaveProperty('title');
      expect(posts).toHaveProperty('postItems');
      expect(posts).toHaveProperty('tags');

      expect(typeof posts.id).toBe('number');
      expect(typeof posts.title).toBe('string');
      expect(posts.postItems).toBeInstanceOf(Array);
      expect(posts.tags).toBeInstanceOf(Array);
    });
  });

  it('Post items of a single post', () => {
    data.postsByTag.postList.forEach((posts) => {
      posts.postItems.forEach((postItem) => {
        expect(postItem).toHaveProperty('id');
        expect(postItem).toHaveProperty('title');
        expect(postItem).toHaveProperty('text');
        expect(postItem).toHaveProperty('description');
        expect(postItem).toHaveProperty('lang');

        expect(typeof postItem.id).toBe('number');
        expect(typeof postItem.title).toBe('string');
        expect(typeof postItem.text).toBe('string');
        expect(typeof postItem.description).toBe('string');
        expect(postItem.lang).toEqual(expect.objectContaining({
          id  : expect.any(Number),
          name: expect.any(String),
          logo: expect.any(String)
        }));
      });
    });
  });

  it('Tags of a single post', () => {
    data.postsByTag.postList.forEach((posts) => {
      posts.tags.forEach((tag) => {
        expect(tag).toHaveProperty('id');
        expect(tag).toHaveProperty('name');

        expect(typeof tag.id).toBe('number');
        expect(typeof tag.name).toBe('string');
      });
    });
  });
});