import { ApolloQueryResult }   from 'apollo-client';
import apiModule               from '~/tests/setup';
import { GetPostByIdResponse } from '~/api/posts/types';

describe('Detail post', () => {
  let response: ApolloQueryResult<GetPostByIdResponse>;
  let data: GetPostByIdResponse;

  beforeAll(async() => {
    response = await apiModule.posts.getPostById(2);
    data = response.data;
  });

  it('Detail post has keys', () => {
    expect(data).toHaveProperty('postById');

    expect(data.postById).toEqual(expect.objectContaining({
      id       : expect.any(Number),
      title    : expect.any(String),
      postItems: expect.any(Array),
      tags     : expect.any(Array)
    }));
  });

  it('Post items of the detail post', () => {
    data.postById.postItems.forEach((postItem) => {
      expect(postItem).toHaveProperty('id');
      expect(postItem).toHaveProperty('title');
      expect(postItem).toHaveProperty('text');
      expect(postItem).toHaveProperty('description');
      expect(postItem).toHaveProperty('lang');

      expect(typeof postItem.id).toBe('number');
      expect(typeof postItem.title).toBe('string');
      expect(typeof postItem.text).toBe('string');
      expect(typeof postItem.description).toBe('string');
      expect(postItem.lang).toEqual(expect.objectContaining({
        id  : expect.any(Number),
        name: expect.any(String),
        logo: expect.any(String)
      }));
    });
  });

  it('Tags of the detail post', () => {
    data.postById.tags.forEach((tag) => {
      expect(tag).toHaveProperty('id');
      expect(tag).toHaveProperty('name');

      expect(typeof tag.id).toBe('number');
      expect(typeof tag.name).toBe('string');
    });
  });
});