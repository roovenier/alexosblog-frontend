import ApolloClient                             from 'apollo-client';
import { InMemoryCache, NormalizedCacheObject } from 'apollo-cache-inmemory';
import { createHttpLink }                       from 'apollo-link-http';
import fetch                                    from 'cross-fetch';

import ApiModule                                from '~/api/index';

require('dotenv').config({ path : './.env' });

const apolloClient = new ApolloClient<NormalizedCacheObject>({
  link : createHttpLink({ uri : process.env.APP_GRAPHQL_URL, fetch }),
  cache: new InMemoryCache()
});

const apiModule = ApiModule(apolloClient);

export default apiModule;