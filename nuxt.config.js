
const env = require('dotenv').config();

export default {
  mode  : 'universal',
  env   : env.parsed,
  server: {
    port: process.env.PORT
  },
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || '',
    meta : [
      { charset : 'utf-8' },
      { name : 'viewport', content : 'width=device-width, initial-scale=1' },
      { hid : 'description', name : 'description', content : process.env.npm_package_description || '' }
    ],
    link: [
      { rel : 'icon', type : 'image/x-icon', href : '/favicon.ico' },
      { rel : 'stylesheet', href : 'https://fonts.googleapis.com/css2?family=Noto+Serif:ital,wght@0,400;0,700;1,400;1,700&display=swap&subset=cyrillic' },
      { rel : 'stylesheet', href : 'https://fonts.googleapis.com/css2?family=Source+Code+Pro:ital,wght@0,400;0,700;1,400;1,700&display=swap&subset=cyrillic' }
    ],
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color : '#fff' },
  /*
  ** Global CSS
  */
  css    : [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~plugins/api-accessor'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxt/typescript-build'
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/svg-sprite',
    '@nuxtjs/apollo'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  apollo: {
    clientConfigs: {
      default: {
        httpEndpoint: process.env.APP_GRAPHQL_URL,
      }
    }
  },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
  }
};
