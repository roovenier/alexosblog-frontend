export interface PageHead {
    title?: string;
}

export interface SwiperSettings {
    slidesPerView?: string | number;
    freeMode?: boolean;
    grabCursor?: boolean;
}