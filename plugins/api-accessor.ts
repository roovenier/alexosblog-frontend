import { Plugin }        from '@nuxt/types';
import { initializeApi } from '~/utils/api';
import ApiModule         from '~/api/index';

const apiAccessor: Plugin = ({ app }) => {
  const client = app.apolloProvider.defaultClient;
  const apiModule = ApiModule(client);

  initializeApi(apiModule);
};

export default apiAccessor;
