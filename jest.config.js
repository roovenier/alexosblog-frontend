module.exports = {
  preset          : 'ts-jest',
  moduleNameMapper: {
    '^~/api/(.*)$'  : '<rootDir>/api/$1',
    '^~/tests/(.*)$': '<rootDir>/tests/$1'
  }
};