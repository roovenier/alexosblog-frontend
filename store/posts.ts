import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import { $api }                                 from '~/utils/api';
import { Post, PostItem }                       from '~/api/posts/types';

export interface PostList extends Post {
  activePostItem: PostItem;
  restPostItems: Array<PostItem>;
}

@Module({
  name        : 'posts',
  namespaced  : true,
  stateFactory: true
})
export default class Posts extends VuexModule {
  posts: Array<Post> = []
  postsSkip = 0
  postsLimit = 10
  isPostsEnd = true
  isLoading = false

  get postList(): Array<PostList> {
    return this.posts.map((post) => {
      const postItems = post.postItems.map((postItem) => {
        let logo = postItem.lang.logo;
        if(logo) {
          logo = `${process.env.APP_API_URL}${postItem.lang.logo}`;
        }
        return {...postItem, lang : {...postItem.lang, logo}};
      });

      const postWithLangs = {...post, postItems};

      return {
        ...postWithLangs,
        activePostItem: postWithLangs.postItems[0],
        restPostItems : postWithLangs.postItems.slice(1)
      };
    });
  }

  @Action({commit : 'SET_POSTS'})
  async getPosts(): Promise<Array<Post>> {
    this.SET_LOADING(true);

    const result = await $api.posts.getPosts({limit : this.postsLimit, skip : this.postsSkip});

    this.SET_POSTS_INFO({isLast : result.data.posts.isLast, postsQuantity : result.data.posts.postList.length});

    return result.data.posts.postList;
  }

  @Action({commit : 'SET_POSTS', rawError : true})
  async getPostsByTag(): Promise<Array<Post>> {
    this.SET_LOADING(true);

    const result = await $api.posts.getPostsByTag({
      tagId: this.context.rootGetters['tags/getTagDetail'].id,
      limit: this.postsLimit,
      skip : this.postsSkip
    });
    if(result.errors || result.data.postsByTag.postList.length === 0) {
      throw new Error();
    }

    this.SET_POSTS_INFO({isLast : result.data.postsByTag.isLast, postsQuantity : result.data.postsByTag.postList.length});

    return result.data.postsByTag.postList;
  }

  @Mutation
  SET_POSTS(posts: Array<Post>): void {
    this.posts = [...this.posts, ...posts];
    this.isLoading = false;
  }

  @Mutation
  SET_POSTS_INFO({isLast, postsQuantity}: {isLast: boolean; postsQuantity: number}): void {
    this.isPostsEnd = isLast;
    this.postsLimit = 5;
    this.postsSkip += postsQuantity;
  }

  @Mutation
  SET_POST_LIST_CURRENT_LANG({postId, langId}: {postId: number; langId: number}): void {
    const post = this.posts.find((post) => post.id === postId);
    if(post) {
      const targetPostItem = post.postItems.find((postItem) => postItem.lang.id === langId);
      const restPostItems = post.postItems.filter((postItem) => postItem.lang.id !== langId);
      if(targetPostItem) {
        post.postItems = [targetPostItem, ...restPostItems];
      }
    }
  }

  @Mutation
  SET_LOADING(value: boolean): void{
    this.isLoading = value;
  }

  @Mutation
  RESET_POSTS_STATE(): void {
    this.posts = [];
    this.postsSkip = 0;
    this.postsLimit = 10;
    this.isPostsEnd = true;
  }
}