import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import { $api }                                 from '~/utils/api';
import { Post, PostItem }                       from '~/api/posts/types';

export interface PostDetail extends Post {
  activePostItem: PostItem;
  restPostItems: Array<PostItem>;
}

@Module({
  name        : 'post-detail',
  namespaced  : true,
  stateFactory: true
})
export default class PostDetailStore extends VuexModule {
  post: Post = {
    id       : 0,
    title    : '',
    postItems: [],
    tags     : []
  }

  get postDetail(): PostDetail {
    const postItems = this.post.postItems.map((postItem) => {
      let logo = postItem.lang.logo;
      if(logo) {
        logo = `${process.env.APP_API_URL}${postItem.lang.logo}`;
      }
      return {...postItem, lang : {...postItem.lang, logo}};
    });
    
    const postWithLangs = {...this.post, postItems};
    
    return {
      ...postWithLangs,
      activePostItem: postWithLangs.postItems[0],
      restPostItems : postWithLangs.postItems.slice(1)
    };
  }

  @Action({commit : 'SET_POST', rawError : true})
  async getPost(postId: number): Promise<Post> {
    const result = await $api.posts.getPostById(postId);
    if(result.errors) {
      throw new Error();
    }
    return result.data.postById;
  }

  @Mutation
  SET_POST(post: Post): void {
    this.post = post;
  }

  @Mutation
  SET_POST_DETAIL_CURRENT_LANG({langId}: {langId: number}): void {
    const targetPostItem = this.post.postItems.find((postItem) => postItem.lang.id === langId);
    const restPostItems = this.post.postItems.filter((postItem) => postItem.lang.id !== langId);
    if(targetPostItem) {
      this.post.postItems = [targetPostItem, ...restPostItems];
    }
  }
}