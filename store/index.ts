import { Store }                  from 'vuex';
import PostsModule                from '~/store/posts';
import PostDetailModule           from '~/store/post-detail';
import TagsModule                 from '~/store/tags';
import { initialiseStores, tags } from '~/utils/store-accessor';

export interface RootState {
    posts: PostsModule;
    postDetail: PostDetailModule;  
    tags: TagsModule;  
}

const initializer = (store: Store<RootState>): void => initialiseStores(store);
export const plugins = [initializer];
export * from '~/utils/store-accessor';

export const actions = {
  async nuxtServerInit(): Promise<void> {
    await tags.getAllTags();
  }
};