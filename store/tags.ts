import { Module, VuexModule, Action, Mutation } from 'vuex-module-decorators';
import { $api }                                 from '~/utils/api';
import { Tag }                                  from '~/api/tags/types';

@Module({
  name        : 'tags',
  namespaced  : true,
  stateFactory: true
})
export default class Tags extends VuexModule {
  tags: Array<Tag> = []
  tagDetail: Tag = {
    id  : 0,
    name: ''
  }

  get getTagDetail(): Tag {
    return this.tagDetail;
  }

  @Action({commit : 'SET_ALL_TAGS'})
  async getAllTags(): Promise<Array<Tag>> {
    const result = await $api.tags.getAllTags();
    return result.data.tags;
  }

  @Mutation
  SET_ALL_TAGS(tags: Array<Tag>): void {
    this.tags = tags;
  }

  @Mutation
  SET_TAG_DETAIL(tag: number): void {
    const foundTag = this.tags.find((item) => item.id === tag);
    if(foundTag) {
      this.tagDetail = foundTag;
    }
  }
}