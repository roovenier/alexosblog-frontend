const dateFormatter = (date: Date): string => {
  const today = new Date(date);
  let dd      = today.getDate().toString();
  let mm      = (today.getMonth() + 1).toString();
  const yyyy  = today.getFullYear();
  
  if (Number(dd) < 10) {
    dd = '0' + dd;
  } 
  if (Number(mm) < 10) {
    mm = '0' + mm;
  }

  return `${dd}.${mm}.${yyyy}`;
};

export default dateFormatter;