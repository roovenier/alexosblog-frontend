import hljs       from 'highlight.js';
import javascript from 'highlight.js/lib/languages/javascript';
import 'highlight.js/styles/github.css';

hljs.registerLanguage('javascript', javascript);

const highlightCode = (): void => {
  const codeList: Array<HTMLElement> = Array.from(document.querySelectorAll('pre > code'));

  codeList.forEach((el) => {
    hljs.highlightBlock(el);
  });
};
  
export default highlightCode;